﻿using IntroASP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IntroASP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiCarController : ControllerBase
    {

        private readonly CRUD_API_RESTContext _restContext;

        public ApiCarController(CRUD_API_RESTContext _restContext)
        {
            this._restContext = _restContext;
        }

        public async Task<List<Carro>> Get()
            => await _restContext.Carros.ToListAsync();
    }
}
