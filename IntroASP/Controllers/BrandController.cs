﻿using IntroASP.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IntroASP.Controllers
{
    public class BrandController : Controller
    {
        private readonly CRUD_API_RESTContext _restContext;

        public BrandController(CRUD_API_RESTContext restContext)
        {
            _restContext = restContext;
        }
        public async Task<IActionResult> Index()
            => View(await _restContext.Productos.ToListAsync());
        
    }
}
