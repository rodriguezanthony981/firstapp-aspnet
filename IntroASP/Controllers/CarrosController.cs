﻿using IntroASP.Models;
using IntroASP.Models.ViewsModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IntroASP.Controllers
{
    public class CarrosController : Controller
    {
        private readonly CRUD_API_RESTContext _restContext;

        public CarrosController (CRUD_API_RESTContext restContext)
        {
            _restContext = restContext;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _restContext.Carros.ToListAsync());
        }
            
       

        public IActionResult Create()
        {
            
            ViewData["Colors"] = new SelectList(_restContext.Carros, "Color", "Color");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CarroViewModel model)
        {
            if (ModelState.IsValid)
            {
                var car = new Carro()
                {
                    Id = model.Id,
                    Marca = model.Marca,
                    Modelo = model.Modelo,
                    Year = int.Parse(model.Year),
                    Color = model.Color,
                };

                _restContext.Add(car);
                await _restContext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Colors"] = new SelectList(_restContext.Carros, "Color", "Color", model.Color);
            return View(model);
        }
    }
}
