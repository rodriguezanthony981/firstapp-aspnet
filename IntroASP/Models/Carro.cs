﻿using System;
using System.Collections.Generic;

namespace IntroASP.Models
{
    public partial class Carro
    {
        public int Id { get; set; }
        public string? Marca { get; set; }
        public string? Modelo { get; set; }
        public int? Year { get; set; }
        public string? Color { get; set; }
    }
}
