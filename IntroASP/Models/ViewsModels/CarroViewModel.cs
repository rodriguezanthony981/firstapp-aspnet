﻿using System.ComponentModel.DataAnnotations;

namespace IntroASP.Models.ViewsModels
{
    public class CarroViewModel
    {
        [Required]
        [Display(Name = "ID:")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Marca:")]
        public string Marca { get; set; }

        [Required]
        [Display(Name = "Modelo:")]
        public string Modelo { get; set; }

        [Required]
        [Display(Name = "Año:")]
        public string Year { get; set; }


        [Required]
        [Display(Name = "Color:")]
        public string Color { get; set; }
    }
}
